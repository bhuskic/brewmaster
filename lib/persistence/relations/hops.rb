# frozen_string_literal: true

module Persistence
  module Relations
    class Hops < ROM::Relation[:sql]
      schema(:hops) do
        attribute :id, Types::Serial
        attribute :manufacturer_id, Types::ForeignKey(:manufacturers)
        attribute :hop_type, ::Types::HopType
        attribute :alpha_acids, Types::Strict::Float
        attribute :name, Types::Strict::String
        attribute :description, Types::Strict::String
        attribute :country, Types::Strict::String
        attribute :substitutes, Types::Strict::String
        attribute :created_at, Types::Strict::Time
        attribute :updated_at, Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :manufacturer
        end
      end
    end
  end
end
