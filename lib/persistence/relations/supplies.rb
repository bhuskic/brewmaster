# frozen_string_literal: true

module Persistence
  module Relations
    class Supplies < ROM::Relation[:sql]
      schema(:supplies) do
        attribute :id,                Types::Serial
        attribute :brewer_id,         Types::ForeignKey(:brewers)
        attribute :grain_id,          Types::ForeignKey(:grains).optional
        attribute :hop_id,            Types::ForeignKey(:hops).optional
        attribute :yeast_id,          Types::ForeignKey(:yeasts).optional
        attribute :adjunct_id,        Types::ForeignKey(:adjuncts).optional
        attribute :quantity,          Types::Strict::Integer
        attribute :created_at,        Types::Strict::Time
        attribute :updated_at,        Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :brewer
          belongs_to :grains
          belongs_to :hops
          belongs_to :yeasts
          belongs_to :adjuncts
        end
      end
    end
  end
end
