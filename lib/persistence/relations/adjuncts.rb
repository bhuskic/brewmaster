# frozen_string_literal: true

module Persistence
  module Relations
    class Adjuncts < ROM::Relation[:sql]
      schema(:adjuncts) do
        attribute :id,          Types::Serial
        attribute :name,        Types::Strict::String
        attribute :description, Types::Strict::String

        primary_key :id

      end
    end
  end
end
