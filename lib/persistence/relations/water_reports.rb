# frozen_string_literal: true

module Persistence
  module Relations
    class WaterReports < ROM::Relation[:sql]
      schema(:water_reports) do
        attribute :id,          Types::Serial
        attribute :origin,      Types::Strict::String
        attribute :status,      Types::Strict::Integer
        attribute :ph,          Types::Strict::Float
        attribute :created_at,  Types::Strict::Time
        attribute :updated_at,  Types::Strict::Time

        primary_key :id

        associations do
          has_many :water_minerals
          has_many :minerals, through: :water_minerals
        end
      end
    end
  end
end
