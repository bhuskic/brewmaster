# frozen_string_literal: true

module Persistence
  module Relations
    class Ingredients < ROM::Relation[:sql]
      schema(:ingredients) do
        attribute :id,                Types::Serial
        attribute :recipe_id,         Types::ForeignKey(:recipes)
        attribute :grain_id,          Types::ForeignKey(:grains).optional
        attribute :hop_id,            Types::ForeignKey(:hops).optional
        attribute :yeast_id,          Types::ForeignKey(:yeasts).optional
        attribute :adjunct_id,        Types::ForeignKey(:adjuncts).optional
        attribute :time_of_addition,  Types::Strict::Integer
        attribute :quantity,          Types::Strict::Integer
        attribute :custom_properties, Types::PG::JSONB.optional
        attribute :created_at,        Types::Strict::Time
        attribute :updated_at,        Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :recipe
          belongs_to :grains
          belongs_to :hops
          belongs_to :yeasts
          belongs_to :adjuncts
        end
      end
    end
  end
end
