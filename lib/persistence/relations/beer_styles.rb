# frozen_string_literal: true

module Persistence
  module Relations
    class BeerStyles < ROM::Relation[:sql]
      schema(:beer_styles) do
        attribute :id,          Types::Serial
        attribute :beer_type,   ::Types::BeerType
        attribute :name,        Types::Strict::String
        attribute :description, Types::Strict::String

        primary_key :id

        associations do
          has_many :recipes
        end
      end
    end
  end
end
