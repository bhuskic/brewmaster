# frozen_string_literal: true

module Persistence
  module Relations
    class Manufacturers < ROM::Relation[:sql]
      schema(:manufacturers) do
        attribute :id, Types::Serial
        attribute :name, Types::Strict::String
        attribute :country, Types::Strict::String

        primary_key :id

        associations do
          has_many :grains
          has_many :yeasts
          has_many :hops
        end
      end
    end
  end
end
