# frozen_string_literal: true

module Persistence
  module Relations
    class Recipes < ROM::Relation[:sql]
      schema(:recipes) do
        attribute :id,            Types::Serial
        attribute :brewer_id,     Types::ForeignKey(:brewers)
        attribute :beer_style_id, Types::ForeignKey(:beer_styles)
        attribute :name,          Types::Strict::String
        attribute :description,   Types::Strict::String
        attribute :batch_size,    Types::Strict::Float
        attribute :created_at,    Types::Strict::Time
        attribute :updated_at,    Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :brewer
          belongs_to :beer_style
          has_many :ingredients
          has_many :grains, through: :ingredients, foreign_key: :grain_id
          has_many :hops, through: :ingredients, foreign_key: :hop_id
          has_many :yeasts, through: :ingredients, foreign_key: :yeast_id
          has_many :adjuncts, through: :ingredients, forein_key: :adjunct_id
        end
      end
    end
  end
end

