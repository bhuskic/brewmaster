# frozen_string_literal: true

module Persistence
  module Relations
    class Brewers < ROM::Relation[:sql]
      schema(:brewers) do
        attribute :id,         Types::Serial
        attribute :firstname,  Types::Strict::String
        attribute :lastname,   Types::Strict::String
        attribute :created_at, Types::Strict::Time
        attribute :updated_at, Types::Strict::Time

        primary_key :id

        associations do
          has_many :recipes
        end
      end
    end
  end
end
