# frozen_string_literal: true

module Persistence
  module Relations
    class Minerals < ROM::Relation[:sql]
      schema(:minerals) do
        attribute :id,              Types::Serial
        attribute :name,            Types::Strict::String
        attribute :symbol,          Types::Strict::String
        attribute :weight,          Types::Strict::Integer
        attribute :created_at,      Types::Strict::Time
        attribute :updated_at,      Types::Strict::Time

        primary_key :id

        associations do
          has_many :water_minerals
          has_many :water_reports, through: :water_minerals
        end
      end
    end
  end
end
