# frozen_string_literal: true

module Persistence
  module Relations
    class WaterMinerals < ROM::Relation[:sql]
      schema(:water_minerals) do
        attribute :id,              Types::Serial
        attribute :water_report_id, Types::ForeignKey(:water_reports)
        attribute :mineral_id,      Types::ForeignKey(:minerals)
        attribute :quantity,        Types::Strict::Float
        attribute :created_at,      Types::Strict::Time
        attribute :updated_at,      Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :water_report
          belongs_to :mineral
        end
      end
    end
  end
end
