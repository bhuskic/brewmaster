# frozen_string_literal: true

module Persistence
  module Relations
    class GrainTypes < ROM::Relation[:sql]
      schema(:grain_types) do
        attribute :id, Types::Serial
        attribute :name, Types::Strict::String
        attribute :description, Types::Strict::String
        attribute :created_at, Types::Strict::Time
        attribute :updated_at, Types::Strict::Time

        primary_key :id

        associations do
          has_many :grains
        end
      end
    end
  end
end
