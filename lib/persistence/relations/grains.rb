# frozen_string_literal: true

module Persistence
  module Relations
    class Grains < ROM::Relation[:sql]
      schema(:grains) do
        attribute :id, Types::Serial
        attribute :grain_type_id, Types::ForeignKey(:grain_types)
        attribute :manufacturer_id, Types::ForeignKey(:manufacturers)
        attribute :name, Types::Strict::String
        attribute :color, Types::Strict::Integer
        attribute :description, Types::Strict::String
        attribute :malt, Types::Bool
        attribute :created_at, Types::Strict::Time
        attribute :updated_at, Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :grain_type
          belongs_to :manufacturer
        end
      end
    end
  end
end
