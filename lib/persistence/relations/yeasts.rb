# frozen_string_literal: true

module Persistence
  module Relations
    class Yeasts < ROM::Relation[:sql]
      schema(:yeasts) do
        attribute :id,                Types::Serial
        attribute :manufacturer_id,   Types::ForeignKey(:manufacturers)
        attribute :yeast_type,        ::Types::YeastType
        attribute :name,              Types::Strict::String
        attribute :profile,           Types::Strict::String
        attribute :from_temperature,  Types::Strict::Integer
        attribute :to_temperature,    Types::Strict::Integer
        attribute :description,       Types::Strict::String
        attribute :created_at,        Types::Strict::Time
        attribute :updated_at,        Types::Strict::Time

        primary_key :id

        associations do
          belongs_to :manufacturer
          has_many :beer_styles
        end
      end
    end
  end
end
