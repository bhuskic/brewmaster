require "brewmaster/view"

module Brewmaster
  module Views
    class Welcome < Brewmaster::View
      configure do |config|
        config.template = "welcome"
      end
    end
  end
end
