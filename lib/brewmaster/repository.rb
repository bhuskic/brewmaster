# auto_register: false

require "rom-repository"
require "brewmaster/container"
require "brewmaster/import"

module Brewmaster
  class Repository < ROM::Repository::Root
    include Import.args["persistence.rom"]
  end
end
