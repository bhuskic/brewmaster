# frozen_string_literal: true

require "types"

module Brewmaster
  class Struct < Dry::Struct
    transform_keys(&:to_sym)

    # resolve default types on nil
    transform_types do |type|
      if type.default?
        type.constructor do |value|
          value.nil? ? Dry::Types::Undefined : value
        end
      else
        type
      end
    end

    def container
      Brewmaster::Container
    end
  end
end
