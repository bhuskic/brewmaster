# frozen_string_literal: true

module Brewmaster
  module Calculators
    class Water

      attr_reader :water_report

      def initialize(**args)
        super(args).reject { |k, v| k == :water_report }
        @water_report = args[:water_report]
      end

      def residual_alkalinity
        total_alkalinity - (calcium_miliequvalents / 3.5 + magnesium_miliequivalents / 7)
      end

      def total_alkalinity
        (calcium_miliequivalents  - magnesium_miliequivalents) * 50
      end

      def termporary_alkalinity
        water_report.calcium + water_report.bicarbonates
      end

      def calicium_miliequivalents
        water_report.calcium / 20
      end

      def magnesium_miliequivalent
        water_report.magnesium / 12.1
      end

      def calcium_sulfate_addition

      end

      def calcium_chloride_addiontion
      end

      def chalk_addition

      end

      def bakin_soda_addition

      end
    end
  end
end
