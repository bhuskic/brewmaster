# frozen_string_literal: true

require "brewmaster/struct"

module Brewmaster
  module Entities
    class GrainType < Brewmaster::Struct
      attribute :id, Types::Strict::Integer
      attribute :name, Types::Strict::String
      attribute :description, Types::Strict::String.default("")
      attribute? :created_at, Types::Time
      attribute? :updated_at, Types::Time
    end
  end
end
