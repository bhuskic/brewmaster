# frozen_string_literal: true

require "dry-struct"
require "dry-types"

Dry::Types.load_extensions(:maybe)

module Types
  include Dry::Types()

  BeerType = Types::Strict::String.constrained(included_in: %w(ale lager)).default("ale")
  YeastType = Types::Strict::String.constrained(included_in: %w(top bottom)).default("top")
  HopType = Types::Strict::String.constrained(included_in: %w(bitterness dual aroma)).default("bitterness")
end
