ROM::SQL.migration do
  change do
    create_table(:ingredients) do
      primary_key :id
      foreign_key :recipe_id,   :recipes, null: false
      foreign_key :grain_id,    :grains
      foreign_key :hop_id,      :hops
      foreign_key :yeast_id,    :yeast
      foreign_key :adjunct_id,  :adjuncts
      Integer :time_of_addition
      Integer :quantity
      jsonb   :custom_properties, default: JSON.generate({})
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      constraint(:ingredient_required) {
        (grain_id + hop_id+ yeast_id + adjunct_id) =~ 1
      }
    end
    run <<-SQL
      ALTER TABLE supplies
      ADD CONSTRAINT supply_item_check
      CHECK (
        (
          (grain_id is not null)::integer +
          (hop_id is not null)::integer +
          (yeast_id is not null)::integer +
          (adjunct_id is not null)::integer 
        ) = 1
      );
    SQL
    run "CREATE UNIQUE INDEX ON ingredients (grain_id) WHERE GRAIN_ID IS NOT NULL"
    run "CREATE UNIQUE INDEX ON ingredients (hop_id) WHERE HOP_ID IS NOT NULL"
    run "CREATE UNIQUE INDEX ON ingredients (yeast_id) WHERE YEAST_ID IS NOT NULL"
    run "CREATE UNIQUE INDEX ON ingredients (adjunct_id) WHERE ADJUNCT_ID IS NOT NULL"
  end
end
