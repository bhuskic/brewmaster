ROM::SQL.migration do
  change do
    create_table(:brewers) do
      primary_key :id
      String :firstname
      String :lastname
      citext :email, null: false
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      constraint :valid_email, email: /^[^,;@ \r\n]+@[^,@; \r\n]+\.[^,@; \r\n]+$/
      constraint :valid_firstname, firstname: /[a-zA-Z0-9]+/
      constraint :valid_lastname, lastname: /[a-zA-Z0-9]+/
      index :email, unique: true
    end
  end
end
