ROM::SQL.migration do
  change do
    create_table(:hops) do
      primary_key :id
      foreign_key :manufacturer_id, :manufacturers
      String :hop_type, null: false, index: true
      Float :alpha_acids, null: false
      String :name, null: false, index: true
      String :description, text: true
      String :country, null: false
      String :substitutes
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      unique [:manufacturer_id, :name, :country]
    end
  end
end
