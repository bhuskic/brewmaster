ROM::SQL.migration do
  change do
    create_table(:manufacturers) do
      primary_key :id
      String :name, null: false
      String :country
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
