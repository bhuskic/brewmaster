ROM::SQL.migration do
  change do
    create_table(:yeast) do
      primary_key :id
      foreign_key :manufacturer_id,  :manufacturers
      String  :yeast_type, null: false, index: true
      String  :name, null: false
      String  :profile
      Integer :from_temperature
      Integer :to_temperature
      String :description
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
