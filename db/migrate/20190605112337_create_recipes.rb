ROM::SQL.migration do
  change do
    create_table(:recipes) do
      primary_key :id
      foreign_key :brewer_id,  :brewers, null: false
      foreign_key :beer_style_id, :beer_styles, null: false
      String :name, index: true
      String :description, text: true
      Float :batch_size, null: false
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
