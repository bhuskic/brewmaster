ROM::SQL.migration do
  change do
    create_table(:grain_types) do
      primary_key :id
      String :name, null: false
      String :description
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
