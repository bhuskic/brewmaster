ROM::SQL.migration do
  change do
    create_table(:beer_styles) do
      primary_key :id
      String :beer_type, null: false, default: "ale"
      String :name, null: false, index: true
      String :description, text: true
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
