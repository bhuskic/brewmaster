ROM::SQL.migration do
  change do
    create_table(:supplies) do
      primary_key :id
      foreign_key :recipe_id,   :recipes, null: false
      foreign_key :grain_id,    :grains
      foreign_key :hop_id,      :hops
      foreign_key :yeast_id,    :yeast
      foreign_key :adjunct_id,  :adjuncts
      Integer :quantity
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
    run <<-SQL
      ALTER TABLE supplies
      ADD CONSTRAINT supply_item_check
      CHECK (
        (
          (grain_id is not null)::integer +
          (hop_id is not null)::integer +
          (yeast_id is not null)::integer +
          (adjunct_id is not null)::integer 
        ) = 1
      );
    SQL
    run "CREATE UNIQUE INDEX ON supplies (grain_id) WHERE GRAIN_ID IS NOT NULL"
    run "CREATE UNIQUE INDEX ON supplies (hop_id) WHERE HOP_ID IS NOT NULL"
    run "CREATE UNIQUE INDEX ON supplies (yeast_id) WHERE YEAST_ID IS NOT NULL"
    run "CREATE UNIQUE INDEX ON supplies (adjunct_id) WHERE ADJUNCT_ID IS NOT NULL"
  end
end
