ROM::SQL.migration do
  change do
    create_table(:grains) do
      primary_key :id
      foreign_key :grain_type_id, :grain_types, null: false
      foreign_key :manufacturer_id, :manufacturers
      String :name, null: false, index: true
      Integer :color, default: 1
      String :description, text: true
      #TrueClass :malt, default: false
      DateTime :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime :updated_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      unique [:grain_type_id, :manufacturer_id, :name]
    end
  end
end
