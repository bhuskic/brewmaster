# frozen_string_literal: true

begin
  require "pry-byebug"
rescue LoadError
end

require_relative "brewmaster/container"

Brewmaster::Container.finalize!

require "brewmaster/web"
