Brewmaster::Container.boot :monitor do
  init do
    require "dry/monitor"
    Dry::Monitor.load_extensions(:sql)
  end

  start do
    notifications.register_event(:sql)
    Dry::Monitor::SQL::Logger.new(logger).subscribe(notifications)
  end
end
