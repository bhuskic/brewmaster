require "dry/web/container"
require "dry/system/components"
require "dry/monitor"

Dry::Monitor.load_extensions(:rack)

module Brewmaster
  class Container < Dry::Web::Container
    configure do
      config.name = :brewmaster
      config.listeners = true
      config.default_namespace = "brewmaster"
      config.auto_register = %w[lib/brewmaster]
    end

    load_paths! "lib"
  end
end
