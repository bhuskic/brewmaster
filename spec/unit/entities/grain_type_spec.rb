# frozen_string_literal: true

require "brewmaster/entities/grain_type"

RSpec.describe Brewmaster::Entities::GrainType do
  subject(:entity) { described_class }
  let(:attributes) {
    {
      id: 1,
      name: "Barley",
      description: "Barley is...",
      created_at: Time.now,
      updated_at: Time.now
    }
  }
  let(:types) { {id: Integer, name: String, created_at: Time, updated_at: Time} }
  let(:required) { %i(id name) }
  let(:defaults) { {description: "", created_at: nil, updated_at: nil}}

  it "is brewmaster entity" do
    expect(entity.new(attributes)).to(
      be_brewmaster_entity
      .with_attributes(attributes)
      .with_types(types)
      .with_required(required)
      .with_defaults(defaults)
    )
  end
end
