# frozen_string_literal: true

RSpec::Matchers.define :be_brewmaster_entity do
  match do |actual|
  @error_message = []
    actual.class.superclass == Brewmaster::Struct &&
      has_attributes?(actual, exp_attributes) &&
      has_types?(actual, exp_types) &&
      has_required?(actual, exp_attributes, exp_required) &&
      has_defaults?(actual, exp_attributes, exp_defaults)
  end

  chain :with_attributes, :exp_attributes

  chain :with_types, :exp_types

  chain :with_required, :exp_required

  chain :with_defaults, :exp_defaults

  failure_message do |actual|
    "#{actual.class.name} doesn't have #{@error_message.join(', ')}"
  end

  def has_attributes?(entity, attributes)
    attributes
      .all? { |method_name, value| entity.send(method_name) == value }
      .tap { |res| @error_message << "all attributes set" unless res }
  end

  def has_types?(entity, attributes)
    attributes
      .all? { |method_name, value| entity.send(method_name).is_a?(value) }
      .tap { |res|
        @error_message << "all correct types for attributes" unless res
      }
  end

  def has_required?(entity, attributes, required)
    required.all? do |attr|
      entity.class.new(attributes.reject { |k, v| k == attr })
      false
    rescue Dry::Struct::Error
      true
    end.tap { |res| @error_message << "all required attributes" unless res }
  end

  def has_defaults?(entity, attributes, defaults)
    defaults.all? do |method_name, default_value|
      new_entity = entity.class.new(attributes.reject { |k, v| k == method_name })
      new_entity.send(method_name) == default_value
    end.tap { |res| @error_message << "default values for attributes" unless res }
  end
end
