module Test
  module DatabaseHelpers
    module_function

    def rom
      Brewmaster::Container["persistence.rom"]
    end

    def db
      Brewmaster::Container["persistence.db"]
    end
  end
end
